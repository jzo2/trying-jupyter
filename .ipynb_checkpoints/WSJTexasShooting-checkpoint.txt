All the victims killed in the mass shooting at a Texas elementary school Tuesday were in the same classroom, a law-enforcement official said, as details about both the victims and how the massacre unfolded continued to emerge.  

According to law-enforcement officials familiar with the investigation, the gunman barricaded himself in a two-room classroom and fired on law enforcement continually through the windows of the classroom. 

The details of the attack were continuing to unfold a day after 19 children and two teachers were killed at Robb Elementary School in Uvalde, Texas, in the deadliest mass shooting in the U.S. this year.

All of the victims have been identified, Lt. Christopher Olivarez, a spokesman for the Texas Department of Public Safety, told CNN Wednesday. 

Police have identified the gunman as Salvador Ramos, a resident of Uvalde, where the school is located. Uvalde, pronounced you-VAL-dee, is a city of around 16,000 located about 80 miles west of San Antonio. The school teaches second- to fourth-grade students.

Members of an elite Border Patrol tactical team known as Bortac responded to the shooting but couldn’t get into the classroom because of a steel door and cinder block construction, according to the officials familiar with the investigation. Meanwhile, the gunman shot at them through the door and walls. 

Bortac members were able to enter the room after getting a master key from the principal, according to the officials. One Bortac agent took rounds to their shield upon entering, a second was wounded by shrapnel. A third killed the suspect. 

Inside, authorities found dead children in multiple piles, according to the officials.

Agents from Bortac, among the most highly trained federal agents, typically track smugglers, serve high-risk warrants and raid stash houses. 

Before the massacre at Robb Elementary, Ramos shot his grandmother, who he lived with, before going to the elementary school, according to police. Mr. Olivarez said the shooter’s grandmother is alive, and that police are working to understand the motive behind the massacre. 

“Everything is still active,” Mr. Olivarez said. “We’re trying to put all the pieces together.”

A law-enforcement official said Ramos legally purchased two assault-style rifles, within a week of his 18th birthday, from the same local store. The U.S. Bureau of Alcohol, Tobacco, Firearms and Explosives requires firearms dealers in border states such as Texas to report the purchase of two or more semiautomatic rifles within five days as part of a program that aims to spot gun trafficking across the Mexican border.

Former and current law-enforcement officials said that buying two rifles on the same day or during the same week wouldn’t necessarily trigger further inquiry. An ATF official declined to say whether Ramos’s purchase was flagged to the agency.

Officials said they believe Ramos acted alone and aren’t pursuing other suspects, according to Pete Arredondo, chief of police for the local school district. The Federal Bureau of Investigation has determined that there is no known connection to terrorism, a law-enforcement official familiar with the investigation said.

At the current death toll, Tuesday’s shooting in Texas was the deadliest at a U.S. school since the slaughter at Sandy Hook Elementary School in Connecticut in 2012, where 20 children and six staff members died. In 2018, an attack at a high school in Parkland, Fla., left 17 students and staff dead. Historically, elementary schools haven’t been the sites of mass shootings with as much frequency as high schools or middle schools.

Before Tuesday’s shooting the massacre at a Buffalo supermarket on May 14 in which 10 people were killed was this year’s worst mass shooting in the U.S.

President Biden addressed the shooting Tuesday night from the White House not long after returning from a five-day trip to Asia.

“To lose a child is like having a piece of your soul ripped away,” he said in a seven-minute speech from the Roosevelt Room.

As he has before, including in Buffalo earlier this month after the mass shooting there, the Democratic president called for gun-control legislation.

“As a nation, we have to ask, when in God’s name are we going to stand up to the gun lobby?” he said. “I am sick and tired of it. We have to act. And don’t tell me we can’t have an impact on this carnage.”

The president ordered the flag at the White House and across federal property to be flown at half-staff until sunset Saturday.

In an address Wednesday, Pope Francis also called for action. “It is time to say enough to the indiscriminate trafficking of arms,” he said. “Let us all commit ourselves so that such tragedies can never happen again.”

Some of the nation’s worst mass shootings have unfolded in Texas in recent years. A 2017 mass shooting at a church in Sutherland Springs, Texas, killed 26 people.

The following year, a 17-year-old opened fire at a high school in Santa Fe, Texas, killing 10 people.

In 2019, a gunman killed 23 and wounded 22 at a Walmart store in El Paso.

On Monday, the FBI released a report on active-shooter incidents in the U.S. in 2021 that showed a 50% increase from 2020. According to the FBI, 103 people were killed and 140 wounded in last year’s 61 active shooting incidents, excluding the shooters.